from tkinter import *
from tkinter import ttk


def calc(key):
    if key == '=':
        str1 = '-+0123456789./*'
        if calc_entry.get()[0] not in str1:
            calc_entry.insert(END, 'Not a num!')
        try:
            result = eval(calc_entry.get())
            calc_entry.insert(END, '=' + str(result))
        except:
            calc_entry.insert(END, ' Error')
    elif key == 'C':
        calc_entry.delete(0, END)
    elif key == '+/-':
        try:
            if calc_entry.get()[0] == '-':
                calc_entry.delete(0)
            else:
                calc_entry.insert(0, '-')
        except IndexError:
            pass
    else:
        if 'Error' in calc_entry.get() or '=' in calc_entry.get():
            calc_entry.delete(0, END)
        calc_entry.insert(END, key)


memory = 0
root = Tk()
root.title('Calc')
button_list = [
    '7', '8', '9', '/', '',
    '4', '5', '6', '*', '',
    '1', '2', '3', '-', '',
    'C', '0', '.', '+', '='
]
row = 1
col = 0
for i in button_list:
    ttk.Button(root, text=i, width=6, command=lambda x=i: calc(x)).grid(row=row, column=col)
    col += 1
    if col > 4:
        col = 0
        row += 1
calc_entry = Entry(root, font='Calibri 14', width=20)
calc_entry.grid(row=0, column=0, columnspan=5)
root.mainloop()
