print('1.')
user_float_num = float(input('Enter REAL num: '))
print(round(user_float_num, 0))
print(round(user_float_num, 2))
print(round(user_float_num, 5))

print('2.')
my_list = [1, 3, 5, 7, 9, 11]
print(my_list)
print(my_list[len(my_list)-1])

print('3.')
a = float(input('enter A num: '))
b = float(input('enter B num: '))
c = float(input('enter C num: '))
formula = input('enter Formula: ')
# Долго думал, как запустить стрингОвые арифметические знаки.. пока не пересмотрел предыдущий урок ;)
res = eval(formula)
print(res)

print('4.')
my_list = [1, 3, 5, 7, 9, 11]
begin = int(input('Enter begin index: '))
end = int(input('Enter end index: '))
print(my_list[begin:end])

print('5.')
user_str = input('Enter SRT: ')
print(user_str)
# user_str = str(set(user_str))  - НЕ РАБОТАЕТ как нужно((
# Изучая встроенные методы Строки понял что больше всего подходит джоин,
# пришлось гуглить, как его юзать с сетом)))
user_str = ''.join(set(user_str))
print(user_str)
print(list(user_str))

print('6.')
set1 = {1, 3, 5, 7, 9, 11, 13, 15}
set2 = {1, 2, 3, 4, 5, 6, 7, 8, 9}
# Про множества пришлось дополнительно читать, и там обнаружил встроенные методы для сравнения двух множеств...
res_set = set1.intersection(set2)
print(res_set)
# Индексов у множества нет - делаем Список у которого индексы есть)
my_list = list(res_set)
print(my_list[0] - my_list[-1])

print('7.')
my_list1 = [1, 3, 5, 7, 9]
my_list2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
res_list = my_list1 + my_list2
print(res_list)
res_list = list(set(res_list))
print(res_list)

print('8.')
my_tuple = (1, 3, 5, 7, 9, 11, 13, 15)
my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print(set(my_tuple).intersection(set(my_list)))
