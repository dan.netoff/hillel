# import my_func_tools as ftools
import time
import traceback
import random
import re
from datetime import datetime, date, timedelta

from my_lib import module_func
# import my_lib.my_module
from os import path, mkdir


def some_method():
    from my_func_tools import (add, some_func)
    return add(1, 2)


if __name__ == '__main__':
    # a = [1, 2, 3, 4, 5, 2]
    # random.shuffle(a)
    # print(a)
    # module_func()
    BASE_PATH = path.dirname(path.abspath(__file__))
    LOGS_PATH = path.join(BASE_PATH, 'logs')
    # if not path.exists(LOGS_PATH):
    #     mkdir(LOGS_PATH)
    #
    # try:
    #     2 / 0
    # except Exception as e:
    #     with open(path.join(LOGS_PATH, 'my_log'), 'a') as log_file:
    #         log_file.write(traceback.format_exc())
    # some_method()

    print(datetime.now().strftime('%d-%m-%Y %A'))

    one_year = date.today() + timedelta(days=366)
    print(one_year)
    # search = re.sub('-?\d+\.?\d+', '', 'qwewqe-123qweq345.23we')
    # search = re.sub('[q-z]{1}', '', 'qweywqe-123()qweq345.23we')
    # print(search)
    result = re.findall('([a-z])(\d+)', 'q1weqwe1233weqweqwe213321')
    print(result)
    # result = re.match('^-?\d+\.?\d+$', '123.0')
    # print(result)
    time.sleep(2)
