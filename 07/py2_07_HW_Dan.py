import re
# 1
date = '19-11-2017 dfasf 19-11-190991'
myDateList = re.findall('((0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[012])-(19|20)\d\d)', date)
for i in myDateList:
    print(i[0])

# 2
myYear = input('Enter year in YYYY format : ')
if myYear.isdigit():
    myYear = int(myYear)
    if myYear % 4 != 0 or (myYear % 100 == 0 and myYear % 400 != 0):
        print("Обычный Год")
    else:
        print("Высокосный Год")

# 3
# -12.09 + 12.09 + 1 + 2 + 3 + 4 + 5 + 6.7 - 6.7 = 11.0
myStr = 'd-12.09s12.09d1,2,3,5,6.7-6.7ads'
myList = re.findall('[-+]?[0-9]*\.?[0-9]+', myStr)
# from str to float
myList = list(map(float, myList))
mySum = sum(myList)
print(mySum)


# 4
def geometry_dash(a=1, q=2, n=10):
    for i in range(1, n+1):
        yield a
        a = a * q


for i in geometry_dash(a=2, q=2, n=13):
    print(i, end=' ')
print('')

# 5
myStr = 'd-12.09s12.09d1,2,3,5,6.7-6.7ads'
myList = re.sub('-.[0-9]*\.?[0-9]+', '', myStr)
print(myList)

# 6
myStr = 'Dan12Netoff12Lviv121most121London11paerty@Харьков232ПесочинДнисочтин'
myList = re.findall('[A-ZА-Я][a-zа-я]+', myStr)
print(myList)
