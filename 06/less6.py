def division(x, y):
    return x / y

c = division(4, 2)
print(c)
x = 10
y = 2
c = division(y=y, x=x)
print(c)

def division2(x, y=2):
    return x / y
c = division2(x=1)
print(c)


def my_method(a=list()):
    a.append(1)
    print(a)

my_method()
my_method()
my_method()

def sort_args(*args):
    print(args)

sort_args(1, 2, 3, 4, 5, 6, 7)

def sort_args(**kvargs):
    print(kvargs)

sort_args(a=1, b=2, c=3)

some = {'a':1, 'b':2, 'c':4, 'd':5, 'e':6, 'f':7}

print(some)

#lambda x, y r: x + y + r

users = [
    {'name': 'Loki', 'age': 29},
    {'name': 'Tor', 'age': 10},
    {'name': 'usuka', 'age': 15}
    ]

users = sorted(users, key=lambda x: x['age'])
print(users)

users = sorted(users, key=lambda x: len(x['name']))
print(users)

a = [1, 2, 3, 4, 5, 6, 7]

a = map(lambda x: x+1, a)

print(list(a))

def endless():
    i = 0
    while True:
        i += 1
        yield i
end_num = endless()
print(next(end_num))
print(next(end_num))
print(next(end_num))

def endless2(_from, to):
    item = _from
    while item <= to:
        yield item
        item += 1
end_num = endless2(10, 20)
print(list(end_num))

def some():
    print(a)


a = [1, 2, 3, 4]
some()














