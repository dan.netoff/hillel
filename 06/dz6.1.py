num = input('Enter num < 4000')
rnum = ''
roman = [[1000, 'M'], [900, 'CM'], [500, 'D'], [400, 'CD'],
         [100, 'C'], [90, 'XC'], [50, 'L'], [40, 'XL'],
         [10, 'X'], [9, 'IX'], [5, 'V'], [4, 'IV'], [1, 'I']]
if num.isdigit():
    anum = int(num)
    i = 0
    while anum > 0:
        while roman[i][0] > anum:
            i += 1
        rnum += roman[i][1]
        anum -= roman[i][0]
print(rnum)
