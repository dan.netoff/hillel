##############
print(1)
my_num = input('Enter num: ')
rom_num = ''
if my_num.isdigit():
    roman = [[1000, 'M'], [900, 'CM'], [500, 'D'], [400, 'CD'],
             [100, 'C'], [90, 'XC'], [50, 'L'], [40, 'XL'],
             [10, 'X'], [9, 'IX'], [5, 'V'], [4, 'IV'], [1, 'I']]
    i = 0
    my_num = int(my_num)
    while my_num > 0:
        while roman[i][0] > my_num:
            i += 1
        rom_num += roman[i][1]
        my_num -= roman[i][0]
print(rom_num)


##############
print(2)


def no_numbers(file_path):
    replace_char = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    with open(file_path, 'r') as file:
        some_text = file.read()
    print('before: \n', some_text)
    for char in replace_char:
        some_text = some_text.replace(char, '')
    print('after: \n', some_text)
    with open(file_path, 'w') as file:
        file.write(some_text)


no_numbers('some2.txt')
#############
print('3-4')
alphabet = ' abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ'


def to_cesar(my_text, secret):
    new_text = ''
    for ch in my_text:
        if ch in alphabet:
            new_text += alphabet[((alphabet.index(ch) + secret) % len(alphabet))]
        else:
            new_text += ch
    return new_text


def from_cesar(my_text, secret):
    new_text = ''
    for ch in my_text:
        if ch in alphabet:
            new_text += alphabet[(((alphabet.index(ch) + len(alphabet)) - secret) % len(alphabet))]
        else:
            new_text += ch
    return new_text


my_text = 'Danil is poor or pure program33r lol Zzz'
secret = 55
my_text = to_cesar(my_text, secret)
print(my_text)
my_text = from_cesar(my_text, secret)
print(my_text)

print(5)
text = ''
with open('some.txt', 'r') as file:
    text = file.read()
    text = to_cesar(text, 5)
with open('new_file.txt', 'w+') as file:
    file.write(text)
