import uuid
import random
import json
from datetime import datetime, timedelta, date

product_names = ('Milk', 'Water', 'Pepsi', 'Rum', 'Jin', 'Becherovka')


class Product:
    def __init__(self, new=None):
        if new is None:
            self.id = uuid.uuid4()
            self.name = None
            self.incomeDate = None
            self.incomePrice = 0
            self.sellPrice = 0
            self.sellDate = None
        else:
            self.id = str(uuid.uuid4())
            self.name = random.choice(product_names)
            self.incomeDate = str(datetime.date((datetime.today()) - timedelta(random.randint(11, 30))))
            self.incomePrice = round(random.uniform(11, 55), 2)
            self.sellPrice = round(self.incomePrice + random.uniform(1, 22), 2)
            self.sellDate = None

    def __repr__(self):
        res = '['
        for key, value in self.__dict__.items():
            res += f'{key} : {value}, '
        return res + ' ]\n'

    # Генератор Дат для будущих отчетов, милая штука
    @staticmethod
    def daterange(date1, date2):
        for n in range(int((date2 - date1).days) + 1):
            yield date1 + timedelta(n)

    @staticmethod
    def fromdict(prodDict):
        prod = Product()
        for key, value in prodDict.items():
            if hasattr(prod, key):
                setattr(prod, key, value)
        return prod

    @staticmethod
    def generate_prodlist(count: int):
        prodlist = []
        for i in range(0, count):
            prod = Product(new=1)
            if random.randint(0, 1) == 1:
                prod.sellDate = str(datetime.date(datetime.today() - timedelta(random.randint(0, 15))))
            prodlist.append(prod)
        return prodlist

    # Сериалиция-Десериализация = я о таких словах слышал)))
    # но как делать правильно с Класами не гуглил.. сделал по своему через __dict__
    # A у даты такого дикта нет;) поэтому у меня type(incomeDate) = str
    # в идеале тут уже БД нужна, для этого уже и есть поля-индексы)))
    @staticmethod
    def package(prodlist):
        return json.dumps(prodlist, default=lambda prod: prod.__dict__)

    @staticmethod
    def unpackage(dump):
        newprodlist = []
        jsonstr = json.loads(dump)
        for prod in list(jsonstr):
            newprod = Product.fromdict(prod)
            newprodlist.append(newprod)
        return newprodlist

    # Текущие остатки, слава фильтру))
    @staticmethod
    def get_remnant_list(prodlist):
        return list(filter(lambda x: x.sellDate is None, prodlist))

    @staticmethod
    def get_sell_report(prodlist, date1, date2):
        selldict = {}
        # Создаем пустой Дикт, с ключами = Датами
        for d in Product.daterange(date1, date2):
            selldict[str(d)] = [0, 0, 0, 0]
        for prod in prodlist:
            if prod.sellDate is not None:
                selldate = datetime.strptime(prod.sellDate, '%Y-%m-%d').date()
                if selldate in Product.daterange(date1, date2):
                    selldict[prod.sellDate][0] += 1
                    selldict[prod.sellDate][1] = round((selldict[prod.sellDate][1] + prod.incomePrice), 2)
                    selldict[prod.sellDate][2] = round((selldict[prod.sellDate][2] + prod.sellPrice), 2)
                    selldict[prod.sellDate][3] = round((selldict[prod.sellDate][3] + prod.sellPrice - prod.incomePrice), 2)
        return selldict

    @staticmethod
    def print_sell_report(selldict):
        sumCount = 0
        sumIncomePrice = 0
        sumSellPrice = 0
        print(' -' * 29)
        print(f'|{"December":^11} | {"Count":>5} | {"IncPrice":>9} | {"SellPrice":>9} | {"Profit":>9} |')
        print(' -' * 29)
        for key, value in selldict.items():
            sumCount += value[0]
            sumIncomePrice += value[1]
            sumSellPrice += value[2]
            print(f'|{key:>11} | {value[0]:>5} | {value[1]:>9.2f} | {value[2]:>9.2f} | {value[3]:>9.2f} |')
        print(' -' * 29)
        print(f'|{"TOTAL":^11} | {sumCount:>5} | {round(sumIncomePrice, 2):>9} | {round(sumSellPrice, 2):>9} | {round(sumSellPrice-sumIncomePrice, 2):>9} |')
        print(' -' * 29)


prodCount = 1000
prodList = Product.generate_prodlist(prodCount)
# Сохраняем данные
prodDumps = Product.package(prodList)
# Загружаем данные
prodList = Product.unpackage(prodDumps)
# print(prodList)

# Список остатков
rem = Product.get_remnant_list(prodList)
print(rem)

# Отчет по продажам за любую дату (за Декабрь)
date_1 = date(2019, 12, 1)
date_2 = date(2019, 12, 31)
Product.print_sell_report(Product.get_sell_report(prodList, date_1, date_2))
