import datetime
import random

authors = ['Mark Twen', 'Jack London', 'Philip Dick', 'Dontsova', 'Habra']
titles = ['book1', 'Pyhton in 5 minutes', 'NeverJava', 'who kill Delphi?', 'book2', 'book3']


def get_random_date(year):
    try:
        return datetime.datetime.strptime('{} {}'.format(random.randint(1, 366), year), '%j %Y').date()
    except ValueError:
        get_random_date(year)


class Book:
    title = None
    author = None
    issue_date = None
    pages = None
    read_date = None
    add_date = None

    def __init__(self, title, author, issue_date, pages, add_date, readed):
        self.title = title
        self.author = author
        self.issue_date = issue_date
        self.pages = pages
        self.add_date = add_date
        if readed == 1:
            self.read_date = self.add_date + datetime.timedelta(random.randint(1, 15))

    def __repr__(self):
        return (f'\n[title = {self.title}, author = {self.author}, issue_date = {self.issue_date}, '
                f'pages = {self.pages} , add_date = {self.add_date} , read_date = {self.read_date} ]')

    @staticmethod
    def get_unread_book():
        return list(filter(lambda x: x.read_date is None, myLibrary))

    @staticmethod
    def get_by_issue_year(year):
        return list(filter(lambda x: x.issue_date.year == year, myLibrary))

    @staticmethod
    def sort_by_issue_date():
        return list(sorted(myLibrary, key=lambda x: x.issue_date))


myLibrary = []
i = 0
while i < 20:
    myBook = Book(
        title=random.choice(titles),
        author=random.choice(authors),
        issue_date=get_random_date(random.randint(2010, 2015)),
        pages=random.randint(50, 1000),
        add_date=(datetime.datetime.now().date() - datetime.timedelta(random.randint(5, 15))),
        readed=random.randint(0, 1)
    )
    myLibrary.append(myBook)
    i += 1

print(myLibrary)

myUnreadBook = Book.get_unread_book()
print(myUnreadBook)

issueYearBook = Book.get_by_issue_year(2010)
print(issueYearBook)

mySortBook = Book.sort_by_issue_date()
print(mySortBook)