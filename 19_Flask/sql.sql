CREATE TABLE songs (
id_songs INTEGER PRIMARY KEY AUTOINCREMENT,
auth_name TEXT NOT NULL,
title TEXT NOT NULL,
count_page INTEGER NOT NULL,
price REAL NOT NULL
);



CREATE TABLE songs (
id_songs INTEGER PRIMARY KEY AUTOINCREMENT,
song TEXT NOT NULL,
artist TEXT NOT NULL,
album TEXT NOT NULL,
duration TEXT NOT NULL,
uploader TEXT NOT NULL,
upload_date TEXT NOT NULL
);

-- Запрос на добавление данных в таблицу

INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('10 Change The Formality.m4a', 'Infected Mushroom', 'Vicious Delicious', '7:44', 'Dan', '24.12.2019');
INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('02 Artillery.m4a', 'Infected Mushroom', 'Vicious Delicious', '4:28', 'Dan', '24.12.2019');
INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('01 Becoming Insane.m4a', 'Infected Mushroom', 'Vicious Delicious', '7:20', 'Dan', '24.12.2019');
INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('04 B.P. Empire.m4a', 'Infected Mushroom', 'B.P. Empire', '7:26', 'Dan', '24.12.2019');
INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('09 Dancing With Kadafi.m4a', 'Infected Mushroom', 'B.P. Empire', '10:22', 'Dan', '24.12.2019');

INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('03. Loyalty.mp3', 'Brian Head Welch', 'Save Me From Myself', '5:07', 'Dan', '24.12.2019');
INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('06. Save Me From Myself.mp3', 'Brian Head Welch', 'Save Me From Myself', '5:44', 'Dan', '24.12.2019');
INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('04. Re-bel.mp3', 'Brian Head Welch', 'Save Me From Myself', '5:40', 'Dan', '24.12.2019');
INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('08. Adonai.mp3', 'Brian Head Welch', 'Save Me From Myself', '5:18', 'Dan', '24.12.2019');
INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('11. Washed By Blood.mp3', 'Brian Head Welch', 'Save Me From Myself', '9:34', 'Dan', '24.12.2019');

INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('01 - Lullaby.mp3', 'Hucci', 'Rose Gold', '4:00', 'Dan', '24.12.2019');
INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('02 - Mafia.mp3', 'Hucci', 'Rose Gold', '4:24', 'Dan', '24.12.2019');
INSERT INTO songs (song, artist, album, duration, uploader, upload_date)
VALUES ('08 - End of the Story.mp3', 'Hucci', 'Rose Gold', '2:52', 'Dan', '24.12.2019');


