import time
from requests import get
import asyncio

async def counter():
    item = 1
    while True:
        print(f'{item}')
        await asyncio.sleep(1)
        item += 1

async def counter5():
    item = 1
    while True:
        if item % 5 == 0:
            print(f'{item} 5555')
        await asyncio.sleep(1)
        item += 1

async def main():
    task = asyncio.ensure_future(counter())
    task1 = asyncio.ensure_future(counter5())
    await asyncio.gather(*[task, task1])

loop = asyncio.get_event_loop()
loop.run_until_complete(main())

count = 5
time1 = time.time()

for item in range(count):
    print(item)
    response = get('https://loremflickr.com/320/240')
    if response.status_code == 200:
        with open(f'./images/img_{item+1}.jpg', 'wb') as img:
            img.write(response.content)
