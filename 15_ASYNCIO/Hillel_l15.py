import time

import aiohttp
from requests import get
import asyncio
#
# async def counter():
#     item = 1
#     while True:
#         print(f'{item}')
#         await asyncio.sleep(1)
#         item += 1
#
# async def counter5():
#     item = 1
#     while True:
#         if item % 5 == 0:
#             print(f'{item}')
#         await asyncio.sleep(1)
#         item += 1
#
# async def main():
#     task = asyncio.ensure_future(counter())
#     task1 = asyncio.ensure_future(counter5())
#     await asyncio.gather(*[task, task1])
#
# loop = asyncio.get_event_loop()
# loop.run_until_complete(main())

count = 2000

time1 = time.time()
#
# for item in range(count):
#     print(item)
#     response = get('https://loremflickr.com/320/240')
#     if response.status_code == 200:
#         with open(f'./images/img_{item+1}.jpg', 'wb') as img:
#             img.write(response.content)


async def fetch_data(item, session):
    async with session.get('https://loremflickr.com/320/240', allow_redirects=True, verify_ssl=False) as response:
        data = await response.read()
        with open(f'./images/img_{item + 1}.jpg', 'wb') as img:
            img.write(data)


async def main(count):
    tasks = []
    async with aiohttp.ClientSession() as session:
        for item in range(count):
            tasks.append(
                asyncio.ensure_future(fetch_data(item, session))
            )
        await asyncio.gather(*tasks)

time1 = time.time()
loop = asyncio.get_event_loop()
loop.run_until_complete(main(count))
print(time.time() - time1)
