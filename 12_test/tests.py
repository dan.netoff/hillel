from unittest import TestCase
from unittest.mock import patch, MagicMock

from py2_test import *


class MyTestCase(TestCase):

    def setUp(self):
        self.cat = Cat(name='Thor')

    def tearDown(self):
        print('tearDown')

    @classmethod
    def setUpClass(cls):
        print('setUpClass')

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def test_simple_add(self):
        expected_value = 10
        value = add(1, 9)
        self.assertEqual(expected_value, value)

    def test_add_with_c(self):
        expected_value = 15
        value = add(10, 10, 2)
        self.assertEqual(expected_value, value)

    # @patch('py2_test.randrange', return_value=1)
    # @patch('py2_test.get_me_some_random', return_value='Cool')
    def test_add_except(self):
        expected_value = 'Cool'
        with patch('py2_test.get_me_some_random', return_value='Cool'):
            value = add(-1, 1, 0)
        self.assertEqual(expected_value, value)

    @patch('py2_test.randrange', return_value=1)
    def test_get_me_some_random_not_cool(self, mock_randrange):
        # print(mock_randrange)
        # some = MagicMock(qwe=MagicMock(id=12))
        # print(some.qwe.id)
        expected_value = 'Not Cool'
        value = get_me_some_random()
        self.assertEqual(expected_value, value)

    @patch('py2_test.randrange', return_value=8)
    def test_get_me_some_random_cool(self, mock_randrange):
        expected_value = 'Cool'
        value = get_me_some_random()
        self.assertEqual(expected_value, value)
        mock_randrange.assert_called_once_with(0, 10)

    def test_hug(self):
        expected_value = 'test'
        cat = MagicMock(color='Black', walk=MagicMock(return_value='test'))
        value = hug(cat)
        self.assertEqual(expected_value, value)

    @patch.object(Cat, 'walk', return_value='test')
    def test_hug_2(self, mock_walk):
        expected_value = 'test'
        cat = Cat(name='Tor', color='Black')
        value = hug(cat)
        self.assertEqual(expected_value, value)


class CatTestCase(TestCase):

    def test_walk(self):
        expected_value = f"Tor is walking now"
        cat = Cat(name='Tor')
        value = cat.walk()
        self.assertEqual(expected_value, value)
