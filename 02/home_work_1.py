print('1.')
username = input('Enter your name: ')
userage = int(input('Enter your age (int): '))
print('Hello', username, 'your age is', userage)

print('2.')
user_num = int(input('Enter your number:'))
print('132 pow is', user_num**132, ' and div by 3 is', user_num % 3)

print('3.')
user_num1 = int(input('Enter First number: '))
user_num2 = int(input('Enter Second number: '))
print('(+) of numbers is', user_num1+user_num2)
print('(-) of numbers is', user_num1-user_num2)
print('(*) of numbers is', user_num1*user_num2)
print('(/) of numbers is', user_num1/user_num2)

print('4.')
num_a = int(input('Enter A number: '))
num_b = int(input('Enter B number: '))
num_c = int(input('Enter C number: '))
print('Result of 2a-8b/(a+b+c)', (2*num_a)-((8*num_b)/(num_a+num_b+num_c)))

print('5.')
user_str = input('Enter str: ')
user_str_count = int(input('Enter str count: '))
print(user_str*user_str_count)

print('6.')
num_1 = 125
num_2 = 437
print(num_1, 'div 2 =', num_1 % 2, ', div 3 =', num_1 % 3, ', div 10 =', num_1 % 10, ', div 22 =', num_1 % 22)
print(num_2, 'div 2 =', num_2 % 2, ', div 3 =', num_2 % 3, ', div 10 =', num_2 % 10, ', div 22 =', num_2 % 22)

print('7.')
user_num1 = int(input('Enter First number: '))
user_num2 = int(input('Enter Second number: '))
print('Whole part a/b is', user_num1//user_num2)

print('8.')
user_str1 = input('Enter 1 str: ')
user_str2 = input('Enter 2 str: ')
user_str3 = input('Enter 3 str: ')
result_str = user_str1+' '+user_str2+' '+user_str3
print(result_str)

print('9.')
first = 15
second = 43
print('first=', first, 'second=', second)
first = first + second
second = first - second
first = first - second
print('first=', first, 'second=', second)
