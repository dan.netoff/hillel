if __name__ == '__main__':
    # lambda x: x ** x
    # my_literal = [x ** 2 for x in range(100)]
    # print(my_literal)

    a = 12  # C++ like code - int a = 12

    test_number = 12

    PI_NUMBER = 3.14

    WINDOW_WIDTH = 1024
    WINDOW_HEIGHT = 768

    test_float = 3.5
    test_complex = 2+3j
    test_str = "asdasdasdqwe231423dsa"
    test_str_2 = 'asdasdasd ds ajdlsa lasdjk '
    # "Can't"
    test_str_3 = """
        sdadsadsasd
        asdasdasd        
    """
    my_super_value = None
    test_bool = True  # or False
    # username = input('Enter your name ')
    # print("Hi!", username)
    # print(2**2*2)
    # user_value = float(input('Enter your number: '))
    # print(user_value**2)

    # print(int("b", 16))

    first_str = "test"
    __second_str = "str"
    print('*' * 40)

    # print(first_str * 10)
    # print(bin(test_number))
    # test = input('Enter your code: ')
    # eval(test)

    # print(globals())
    # print(len("qweqwe"))
    # print(round(3.4334523434, 3))
    # print(int(round(3.55, 0)))

    # print(complex(12+3j).imag)
    print(2**3**2)
