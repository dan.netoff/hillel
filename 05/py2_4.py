import io
import traceback

if __name__ == '__main__':
    # my_file = open('some.txt', 'r+')
    # my_file.seek(10)
    # print(my_file.readlines())
    # print('-' * 20)
    # print(my_file.readline())
    # print(my_file.readline())
    # print(my_file.readline())
    # print(my_file.read())
    # my_file.seek(10)
    # my_file.write('Some new data')
    # print(my_file.tell())
    # my_file.writelines(['asd', 'asd', 'asd', 'asd'])
    # 2 / 0
    # my_file.close()
    # for x in range(0, 20):
    #     if x > 10:
    #         2 / 0
    # cursor = 0
    # with open('some.txt', 'r+') as my_file:
    #     print(my_file.readline())
    #     cursor = my_file.tell()
    #     # print(my_file)
    #
    # with open('some.txt', 'r+') as my_file:
    #     my_file.seek(cursor)
    #     print(my_file.readline())
    # try:
    #     with open('some.txt', 'w') as my_file:
    #         my_file.write('TEST')
    #
    #     2 / 0
    #     # a = []
    #     # a[100]
    #
    # except io.UnsupportedOperation:
    #     # traceback.print_exc()
    #     print("Can't write to file")
    #
    # except ZeroDivisionError:
    #     print('Infinity')
    #
    # else:
    #     print('Well done!')
    #
    # finally:
    #     print("I'm done")

    # value = "2w"
    #
    # class MySuperError(Exception):
    #     pass
    #
    # try:
    #     if not value.isdigit():
    #         raise MySuperError('Only digit values accepted')
    # except Exception:
    #     try:
    #         2 / 0
    #     except ZeroDivisionError:
    #         print('Ups!')

    # try:
    #     value = int(value)
    # except TypeError:
    #     pass

    # try:
    #     User.objects.get(id=1)
    # except User.DoesNotExists:
    #     User.objects.create()
    #
    #
    # if User.objects.filter(id=1).exists():
    #     user = User.objects.get(id=1)
    # else:
    #     User.objects.create()

    # with 'qwe' as a:
    #     print(a)

    with open('some.txt', 'r+') as my_file:
        # data = my_file.read()
        # print(my_file.writable())
        while True:
            if my_file.readline() == 'MY STR\n':
                # cursor = my_file.tell() - len('MY STR')
                my_file.seek(my_file.tell())
                my_file.write('test\n')
                break
    # print(data)
    2 / 0
