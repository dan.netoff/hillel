class Car:
    model = None
    color = None
    speed = None
    isLaunched = False

    def __init__(self, model, color, speed):
        self.model = model
        self.color = color
        self.speed = speed

    def launch(self, key=True):
        self.isLaunched = key

    def ride(self, step=1):
        if self.isLaunched:
            print(f'Car {self.model}, color {self.color}, move on Ground {step} km with speed {self.speed} km/h')
        else:
            print('Launch first!')


class Plane:
    model = None
    color = None
    speed = None
    isLaunched = False

    def __init__(self, model, color, speed):
        self.model = model
        self.color = color
        self.speed = speed

    def launch(self, key=True):
        self.isLaunched = key

    def fly(self, step=1):
        if self.isLaunched:
            print(f'Fly {self.model}, color {self.color}, fly on sky {step} km with speed {self.speed} km/h')
        else:
            print('Launch first!')


class Ship:
    model = None
    color = None
    speed = None
    isLaunched = False

    def __init__(self, model, color, speed):
        self.model = model
        self.color = color
        self.speed = speed

    def launch(self, key=True):
        self.isLaunched = key

    def swim(self, step=1):
        if self.isLaunched:
            print(f'Ship {self.model}, color {self.color}, swim on water {step} km with speed {self.speed} km/h')
        else:
            print('Launch first!')


class Amphibian(Car, Plane, Ship):
    def __init__(self, *args, **kwargs):
        super(Amphibian, self).__init__(*args, **kwargs)


car1 = Car(model='Porshe Couen', color='Black', speed=100)
plane1 = Plane(model='SuperJet', color='White', speed=450)
ship1 = Ship(model='CruizLaner', color='JustForFun', speed=30)

car1.launch()
car1.ride(10)

plane1.launch()
plane1.fly(100)

ship1.launch()
ship1.swim(350)

ampha1 = Amphibian(model='Amhibi', color='Black', speed=100)
ampha1.launch()
ampha1.ride(100)
ampha1.swim(122)
ampha1.fly(112)
