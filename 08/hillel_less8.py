import pickle
import time
import os


# my_var = [1, {'name': 'Dan'}, 4]
# with open('some.dump', 'wb') as fd:
#     pickle.dump(my_var, fd)
# with open('some.dump', 'rb') as fd:
#     f = pickle.load(fd)
#     print(f)
def cache(func):
    
    def wrapper(*args, **kwargs):
        cache_name = f'{"_".join([str(x) for x in args])}'
        if os.path.exists(cache_name):
            with open(cache_name, 'rb') as fd:
                return pickle.load(fd)
        data = func(*args, **kwargs)
        with open(cache_name, 'wb') as fd:
            pickle.dump(data, fd)
        return data

    return wrapper


@cache
def some_func(x, y):
    time.sleep(10)
    return x + y


@cache
def some_func2(x, y):
    time.sleep(10)
    return x + y


print(some_func(2, 2))
print(some_func2(2, 2))
