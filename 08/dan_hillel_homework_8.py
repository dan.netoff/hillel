import time
from datetime import datetime


# 1
def stopwatch(start_func):
    def wrapper(*args, **kwargs):
        t1 = datetime.now()
        data = start_func(*args)
        t2 = datetime.now()
        print('Func running time : ', (t2 - t1))
        return data
    return wrapper


@stopwatch
def my_func(sec: int):
    time.sleep(sec)


print('hello')
my_func(2)


# 2
def multiples(count, divider=3):
    my_nums = []
    num = 1
    while len(my_nums) < count:
        if (num % divider) == 0:
            my_nums.append(num)
        num += 1
    return my_nums


my_len = input('ВВедите длину Масива чисел кратные 3: ')
if my_len.isdigit():
    my_list = multiples(int(my_len))
    my_list.reverse()
    print(my_list)
else:
    print('sorry, bad input')


# 3
def skip(res=0):
    def inner(start_func):
        def wrapper(*args, **kwargs):
            return res
        return wrapper
    return inner


@skip(res=777)
def func1():
    return 666


print(func1())


# 4
def triangular_numbers(count: int):
    if count > 0:
        res = [0]
        i = 1
        while i < count:
            res.append(res[-1]+i)
            i += 1
    return res


def triangular_generator(count: int):
    my_list = triangular_numbers(count)
    for i in my_list:
        yield i


my_len = input('ВВедите длину Масива Треугольных чисел ')
if my_len.isdigit():
    my_list = triangular_generator(int(my_len))
    for i in my_list:
        print(i)
else:
    print('sorry, bad input')

