import pprint
import time


class A:

    def __hash__(self):
        return 123


if __name__ == '__main__':
    some = A()
    my_dict = {
        "name": "Loki",
        "username": "Loki",
        "age": 29,
        "cats": ['Tor', 'Magnus'],
        1: "Some",
        1.2: "Some",
        (1, 2): "Some",
        frozenset([1, 3]): "Some more",
        True: "qwe",
        some: 'asdasd'
    }
    my_list = ['Loki', 29, ('Tor', 'Magnus')]
    # print(pprint.pprint(my_dict))
    some = 1
    fromkeys_dict = dict.fromkeys(['Name', 'age'], 0)

    # pprint.pprint(list(my_dict.items()))
    # print(my_dict.get('name', 'No such item'))
    # print(my_dict.popitem())
    my_dict2 = {
        'name': 'Thor',
        'skill': 'lightning',
    }
    # print(my_dict.setdefault('name', 'username'))
    my_dict.update(my_dict2)
    # pprint.pprint(my_dict.keys())
    # pprint.pprint(my_dict.values())
    # zip_dict = zip(
    #     ['name', 'Loki'],
    #     ['age', 29]
    # )
    # dict_from_tuple = dict([
    #     ['name', 'Loki'],
    #     ['age', 29]
    # ])
    # print(dict_from_tuple)
    # a = 1000
    # b = 1000
    # print(a == b)
    # print(a is None)
    # simofor = False
    # if 1 > 0:
    #     print('If block')
    #     if True:
    #         print('True')
    # elif 2 > 0:
    #     print('Elif block')
    # else:
    #     print('Else block')
    # i = 0
    # while i < 10:
    #     print(i)
    #     time.sleep(1)
    #     i += 1
    # for (int i=0; i<10; i++)

    first = 10
    second = 23

    first, second = second, first

    # for key, value in my_dict.items():
    #     print(f"{key}: {value}")

    # for value in range(100):
    #     print(value)

    # my_list = list(range(20))
    # i = 1
    # while i < len(my_list):
    #     if i % 30 == 0:
    #         i += 1
    #         break
    #     print(my_list[i])
    #     i += 1
    # else:
    #     print('Break not happened')
    # my_list = ['Loki', 'Odin', 'Thor', 'Bohdan']
    # for num, item in enumerate(my_list):
    #     print(num, item)

    # some_str = "My mega string!"
    # my_iter = iter("My mega string!")
    # print(my_iter)
    # for letter in my_iter:
    #     print(letter)

    # some_str = iter("My")
    # print(next(some_str))
    # print(next(some_str))
    # print(next(some_str))

    # even = [x for x in range(1000)]
    # even = list(range(1000))
    # print([x for x in even if x % 2 == 0])
    #
    # a = 10
    # b = 20
    # c = 40 if a > b else a + b
    #
    # a = None
    #
    # c = a or b or c or my_dict
    # print(c)
    # even_new = []
    # for item in even:
    #     even_new.append(str(item))
    # even = even_new
    # print((str(x) for x in even))
    # print([str(x) for x in even])
    # print('_'.join((str(x) for x in even)))

# def some():
#     print('Hello world!')
#
#
# d = {
#     'method': some
# }

# print(d)
# d['method']()
