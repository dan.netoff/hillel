print('1.')
my_str = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
Nam quis tempus risus, vel auctor metus. 
Vestibulum ante ipsum primis in faucibus orci luctus et ultrices 
posuere cubilia Curae; Nunc laoreet elit eget auctor sollicitudin. 
Proin porta vitae neque sed placerat. Proin vitae purus tristique, 
congue purus id, posuere ante. Praesent sed sapien quis libero 
ultricies iaculis quis sagittis lacus. Duis dignissim eleifend purus, 
et hendrerit ligula molestie nec. Quisque ac justo et nisi consectetur 
vehicula euismod ac neque. Nulla facilisi. Cras luctus scelerisque mollis. 
Maecenas ac vestibulum metus. Sed finibus vulputate sapien ac consequat. 
Vestibulum faucibus dignissim turpis vel placerat. Morbi lobortis nunc nec 
massa sollicitudin, ac porta ex feugiat.
"""
words = len(my_str.split(' '))
sentenses = len(my_str.split('.'))
print(f'Words in text = {words}')
print(f'Sentenses in text = {sentenses}')

print('2.')
user_num = input('Enter Integer : ')
if user_num.isdigit():
    if int(user_num) % 2 == 0:
        print('Even')
    else:
        print('Odd')
else:
    print('Incorrect input. Bad user!')

print('3.')
user_list = input('Enter value with coma: ')
user_list = user_list.replace(' ', '')
my_list = user_list.split(',')
print(my_list)
incorrect_count = 0
for my_value in my_list:
    if not my_value.isdigit():
        incorrect_count += 1
if incorrect_count != 0:
    print('Sorry incorrect input')
else:
    if len(my_list) < 10:
        my_list.sort(reverse=True)
    elif len(my_list) > 10:
        my_list.sort()
    elif len(my_list) == 10:
        print('What can i do?')
    print(my_list)


print('4.')
user_num1 = input('Enter FIRST Integer : ')
user_num2 = input('Enter SECOND Integer : ')
if user_num1.isdigit() and user_num2.isdigit():
    num1 = int(user_num1)
    num2 = int(user_num2)
    if num1 > num2:
        print('Diff: ', num1 - num2)
    elif num1 < num2:
        print('Sum: ', num1 + num2)
    elif num1 == num2:
        print('Power: ', num1 ** num2)
else:
    print('Incorrect input. Bad user!')


print('5.')
user_num1 = input('Enter FIRST Integer : ')
user_num2 = input('Enter SECOND Integer : ')
user_num3 = input('Enter THIRD Integer: ')
if user_num1.isdigit() and user_num2.isdigit() and user_num3.isdigit():
    a = int(user_num1)
    b = int(user_num2)
    c = int(user_num3)
    if(a-b+c) == 0:
        print('Sorry A-B+C=0, cant ZeroDivision')
    else:
        print(eval('(2*a - 8*b) / (a-b+c)'))
else:
    print('Incorrect input. Bad user!')

print('6.')
my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
even_count = 0
for my_num in my_list:
    if my_num % 2 == 0:
        even_count += 1
print(f'Even count in List = {even_count}')

print('7.')
my_list = [1, 4, 7, 9, 2, 4, 6, 8, 10, -1, -5]
sorted_list = []
for i in range(len(my_list)):
    my_min = min(my_list)
    sorted_list.append(my_min)
    my_list.remove(my_min)
print(sorted_list)

print('8.')
user_num1 = input('Enter Fibonachi length : ')
if user_num1.isdigit():
    if int(user_num1) < 2:
        print('Sorry LENGTH is very small')
    else:
        my_list = [0, 1]
        for i in range(int(user_num1)-2):
            my_list.append(my_list[-2] + my_list[-1])
        print(l)
else:
    print('Incorrect input. Bad user!')

print('9.')
user_num = input('Enter Number : ')
if user_num.isdigit():
    num1 = int(user_num)
    simple = 0
    for i in range(1, num1+1):
        if num1 % i == 0:
            simple += 1
    if simple <= 2:
        print('Num is Simple')
    else:
        print('Num is NOT simple')
else:
    print('Incorrect input. Bad user!')
