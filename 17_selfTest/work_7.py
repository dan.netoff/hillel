from datetime import datetime
import time
import calendar

print('1')
try:
    my_file = open('some.txt', 'r')
    my_str = my_file.read()
except FileNotFoundError:
    print('File not Found :(')
    my_str = """Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Nam quis tempus risus, vel auctor metus.
Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
posuere cubilia Curae; Nunc laoreet elit eget auctor sollicitudin.
Proin porta vitae neque sed placerat. Proin vitae purus tristique,
congue purus id, posuere ante. Praesent sed sapien quis libero
ultricies iaculis quis sagittis lacus. Duis dignissim eleifend purus,
et hendrerit ligula molestie nec. Quisque ac justo et nisi consectetur
vehicula euismod ac neque. Nulla facilisi. Cras luctus? scelerisque mollis.
Maecenas ac vestibulum metus. Sed finibus! vulputate sapien ac consequat.
Vestibulum faucibus dignissim turpis vel placerat. Morbi lobortis nunc nec
massa sollicitudin, ac porta ex feugiat.
"""
words = my_str.count(' ')
sentenses = my_str.count('.') + my_str.count('!') + my_str.count('?')
print(f'Words in text = {words}')
print(f'Sentenses in text = {sentenses}')


print('2')
test_date = 'MM-DD-YYYY'
validDate = False
while not validDate:
    myDate = input('Enter a valid date format MM-DD-YYYY: ')
    try:
        d = datetime.strptime(myDate, "%m-%d-%Y")
        validDate = True
    except ValueError:
        print('Wrong date $(')
myWeekDay = calendar.day_name[d.weekday()]
print(myWeekDay)

print('3')


def stopwatch(start_func):
    def wrapper(*args, **kwargs):
        t1 = datetime.now()
        data = start_func(*args)
        t2 = datetime.now()
        print('Func running time : ', (t2 - t1))
        return data
    return wrapper


@stopwatch
def my_func(sec: int):
    print('func begin')
    time.sleep(sec)
    print('func end')


my_func(10)
